package view.interfaces;

import java.util.List;
import model.players.Decoder;

/**
 * 
 * @author Stefano Benelli
 * This is the basic representation of StandingsView
 */
public interface StandingsView extends View<List<Decoder>> {

}
